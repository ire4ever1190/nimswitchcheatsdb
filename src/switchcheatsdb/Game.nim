import Cheat

type
    Game* = object
        slug*: string
        name*: string
        image*: string
        titleid*: string
        cheats*: seq[Cheat]
