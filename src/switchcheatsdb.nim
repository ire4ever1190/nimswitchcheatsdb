import httpClient
import strformat
import json
import switchcheatsdb/Cheat

type
    Game* = object
        slug*: string
        name*: string
        image*: string
        titleid*: string
        cheats*: seq[Cheat]

let client = newHttpClient()
client.headers = newHttpHeaders(
        {
         "Content-Type": "application/json",
         "X-API-TOKEN": "7da8890e-af7b-44bf-a88a-2375b8070a32"
}
)


# Internal function to send request to switchcheatsdb
proc apiCall(route: string): string =
    return client.request(fmt"http://www.switchcheatsdb.com/api/v1{route}").body


# Gets cheats for a game with a certain title ID
proc getGame*(titleId: string): Game = 
    let response = apiCall(fmt"/cheats/{titleId}")
    let json = parseJson(response)
    return to(json, Game)


