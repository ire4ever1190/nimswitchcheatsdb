# Package

version       = "0.1.0"
author        = "Jake Leahy"
description   = "A nim package to connect to switchcheatsdb"
license       = "MIT"
srcDir        = "src"



# Dependencies

requires "nim >= 1.2.0"

# Compiler settings
switch("d", "ssl")


